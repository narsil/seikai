import React, { useContext, useEffect, useState } from 'react'

import _hiraganaForLearning from './json/hiragana-for-learning.json'
import _katakanaForLearning from './json/katakana-for-learning.json'
import _hiragana from './json/hiragana.json'
import _katakana from './json/katakana.json'
import _kanji_numbers from './json/kanji-numbers.json'
import _kanji_study from './json/kanji-study.json'
import _kanji_calendar from './json/kanji-calendar.json'

const CollectionsContext = React.createContext()

function processData(data = []) {
  return data.reduce((acc, item) => {
    const idx = acc.index.findIndex(f => f.label === item.group)

    if (idx === -1) {
      acc.index.push({ label: item.group, options: [{ label: item.name , value: item.code }] })
    } else {
      acc.index[idx].options.push({ label: item.name , value: item.code })
    }

    acc.data[item.code] = item.data

    return acc
  }, {index: [], data: {}})
}

export const useCollections = () => useContext(CollectionsContext)

export const CollectionsProvider = ({ children }) => {
  const [stateCollections, setCollections] = useState({ index: [], data: {} })

  useEffect(() => {
    setCollections(processData([
      _hiragana,
      _katakana,
      _kanji_numbers,
      _kanji_study,
      _kanji_calendar
    ]))
  }, [])

  return (
    <CollectionsContext.Provider value={{
      collections: stateCollections,
      learningCollections: {
        hiragana: _hiraganaForLearning,
        katakana: _katakanaForLearning
      }
    }}>
      { children }
    </CollectionsContext.Provider>
  )
}