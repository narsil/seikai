import { useEffect, useState } from 'react'
import { Helmet } from 'react-helmet'
import {
  Autocomplete, TextField,
  FormControl,
  ToggleButtonGroup, ToggleButton,
  Button
} from '@material-ui/core'
// import cn from 'classnames'

import { CreateCardElements } from '../cards/CreateCardElements'
import { TestingCard } from '../cards/TestingCard'
import { ResultsTable } from '../components/ResultsTable'
import { pickElement } from '../helpers/array'
import { useCollections } from '../collections/CollectionsProvider'

export default function Testing() {
  const [stateMode, setMode] = useState('kana')
  const [stateTranslation, setTranslation] = useState(false)
  const [stateDifficulty, setDifficulty] = useState(2)
  const [stateCollectionsValue, setCollectionsValue] = useState([])
  const [stateGeneralCollection, setGeneralCollection] = useState([])
  const [stateNumberOfQuestions, setNumberOfQuestions] = useState(null)
  
  const [stateTestingCollection, setTestingCollection] = useState([])
  const [stateCardElements, setCardElements] = useState(null)
  const [stateCards, setCards] = useState([])
  const [stateAnswers, setAnswers] = useState([])
  
  const [stateCurrentQuestion, setCurrentQuestion] = useState(null)
  const [stateCorrectAnswers, setCorrectAnswers] = useState(0)
  
  const [stateSetup, setSetup] = useState(true)
  const [stateTest, setTest] = useState(false)
  const [stateResult, setResult] = useState(false)

  const { collections } = useCollections()

  const mode = {
    'kana': 'Kana',
    'kanji': 'Kanji',
  }
  const translation = new Map([
    [false, 'Disabled'],
    [true, 'Enabled']
  ])
  const difficulty = {
    2: 'Easy',
    3: 'Medium',
    4: 'Hard',
  }

  /* Generate testing collection */
  useEffect(() => {
    if (stateTest) {
      let collection = []
  
      for (let i = 0; i < stateNumberOfQuestions; i++) {
        collection.push(pickElement(stateGeneralCollection, collection))
      }

      setTestingCollection(collection)
      setCurrentQuestion(0)
    }
  }, [stateTest /*  */, stateNumberOfQuestions, stateGeneralCollection])

  /* Generate question card */
  useEffect(() => {
    if (stateTest /* && stateCurrentQuestion !== null */) {
      const pick = stateTestingCollection[stateCurrentQuestion]
      let picks = [pick]
  
      for (let i = 0; i < stateDifficulty - 1; i++) {
        picks.push(pickElement(stateGeneralCollection, picks));
      }

      const cardData = new CreateCardElements({ picks, mode: stateMode, spelling: 'native' })
      const testingCardElements = cardData.exportTestingCardElements({ translation: stateTranslation })
      // const resultsCardElements = cardData.exportResultsCardElements()
  
      setCardElements(testingCardElements)
      setCards(state => [...state, cardData])
    }
  }, [stateCurrentQuestion /*  */, stateTestingCollection, stateDifficulty, stateGeneralCollection, stateMode/* , stateTest */])

  /* Validate number of question */
  useEffect(() => {
    if (stateGeneralCollection.length < stateNumberOfQuestions) {
      setNumberOfQuestions(null)
    }
  }, [stateGeneralCollection /*  */, stateNumberOfQuestions])

  function checkAnswerHandler(isCorrect, idx) {
    setAnswers(state => [...state, idx])
    
    if (isCorrect) {
      setCorrectAnswers(stateCorrectAnswers + 1)
    }
    
    if (stateCurrentQuestion < stateTestingCollection.length - 1) {
      setCurrentQuestion(stateCurrentQuestion + 1)
    } else {
      setTest(false)
      setResult(true)
    }
  }

  function resetHandler() {
    setSetup(true)
    setTest(false)
    setResult(false)

    setCardElements(null)
    setCards([])
    setAnswers([])
    
    setCurrentQuestion(null)
    setCorrectAnswers(0)
  }

  return (
    <>
      <Helmet>
        <title>正解 (Seikai) | Testing</title>
      </Helmet>
      {
        stateResult &&
          <>
            <div className='w-full md:w-1/2 lg:w-1/3 mx-auto mb-8'>
              <div className='mb-8'>
                <div className='text-center text-xl'>Your score:</div>
                <div className='text-center text-6xl text-indigo-600'>{ stateCorrectAnswers } / { stateTestingCollection.length }</div>
              </div>
              <Button
                variant='outlined'
                color='primary'
                className='w-full'
                onClick={resetHandler}
              >
                <span className='text-xl'>Try again</span>
              </Button>
            </div>
            <h2 className='text-3xl text-center mb-4'>Details</h2>
            <div className='table mx-auto mb-4 px-2 py-1 bg-gradient-to-r from-blue-50 to-blue-200 rounded-md text-blue-700'>
              <p>Mode:&nbsp;<b>{ mode[stateMode] }</b></p>
              <p>Translation:&nbsp;<b>{ translation.get(stateTranslation) }</b></p>
              <p>Difficulty:&nbsp;<b>{ difficulty[stateDifficulty] }</b></p>
              <p>
                Collections:&nbsp;
                <b>
                {
                  stateCollectionsValue.reduce((acc, item) => {
                    acc.push(item.label)
                    return acc
                  }, []).join(', ')
                }
                </b>
              </p>
              <p>Number of qustions:&nbsp;<b>{ stateNumberOfQuestions }</b></p>
            </div>
            <ResultsTable
              cards={ stateCards }
              answers={ stateAnswers }
            />
          </>
      }
      {
        stateTest &&
          <div className='relative w-full md:w-1/2 lg:w-1/3 mx-auto'>
            <div className='absolute w-full -top-1'>
              <p className='table mx-auto px-2 py-1 rounded-md border border-white bg-gradient-to-r from-green-200 to-blue-200 text-md text-blue-900'>Question #{ stateCurrentQuestion + 1 }</p>
            </div>
            {
              stateCardElements !== null &&
                <TestingCard
                  data={stateCardElements}
                  checkAnswerHandler={checkAnswerHandler}
                />
            }
          </div>
      }
      {
        stateSetup &&
          <div className='w-full md:w-1/2 lg:w-1/3 mx-auto'>
            <div className='mb-4'>
              <FormControl className='w-full'>
                <label className='mb-1 text-sm text-gray-500'>Mode</label>
                <ToggleButtonGroup
                  value={stateMode}
                  exclusive
                  onChange={(event, value) => setMode(value)}
                  aria-label='mode'
                  className='w-full'
                >
                  <ToggleButton value='kana' aria-label='kana mode' className='w-full'>
                    仮名
                  </ToggleButton>
                  <ToggleButton value='kanji' aria-label='kanji mode' className='w-full'>
                    漢字
                  </ToggleButton>
                </ToggleButtonGroup>
              </FormControl>
            </div>

            <div className='mb-4'>
              <FormControl className='w-full'>
                <label className='mb-1 text-sm text-gray-500'>Translation</label>
                <ToggleButtonGroup
                  value={stateTranslation}
                  exclusive
                  onChange={(event, value) => setTranslation(value)}
                  aria-label='translation'
                  className='w-full'
                >
                  <ToggleButton value={false} aria-label='translation disabled' className='w-full'>
                    Disabled
                  </ToggleButton>
                  <ToggleButton value={true} aria-label='translation enabled' className='w-full'>
                    Enabled
                  </ToggleButton>
                </ToggleButtonGroup>
              </FormControl>
            </div>

            <div className='mb-10'>
              <FormControl className='w-full'>
                <label className='mb-1 text-sm text-gray-500'>Difficulty</label>
                <ToggleButtonGroup
                  value={stateDifficulty}
                  exclusive
                  onChange={(event, value) => setDifficulty(value)}
                  aria-label='difficulty'
                  className='w-full'
                >
                  <ToggleButton value={2} aria-label='easy' className='w-full'>
                    Easy
                  </ToggleButton>
                  <ToggleButton value={3} aria-label='medium' className='w-full'>
                    Medium
                  </ToggleButton>
                  <ToggleButton value={4} aria-label='hard' className='w-full'>
                    Hard
                  </ToggleButton>
                </ToggleButtonGroup>
              </FormControl>
            </div>

            <div className='mb-4'>
              <FormControl
                variant='outlined'
                className='w-full'
              >
                <Autocomplete
                  multiple
                  disableCloseOnSelect
                  autoHighlight
                  options={
                    collections.index.filter(group => group.label !== 'ABC').reduce((acc, group) => ([
                      ...acc,
                      ...group.options.map(
                        (option, idx) => ({
                          label: option.label, value: option.value, group: group.label
                        })
                      )
                    ]), [])
                  }
                  groupBy={ option => option.group }
                  getOptionLabel={ option => option.label }
                  getOptionSelected={ (option, value) => option.value === value.value }
                  renderInput={ params => <TextField {...params} label='Collections' /> }
                  value={ stateCollectionsValue || '' }
                  onChange={(event, value) => {
                    setCollectionsValue(value)
                    setGeneralCollection(
                      value.reduce((acc, item) => {
                        return acc.concat(collections.data[item.value])
                      }, [])
                    )
                  }}
                />
              </FormControl>
            </div>

            <div className='mb-8'>
              <FormControl className='w-full'>
                <label className='mb-1 text-sm text-gray-500'>Number of questions</label>
                <ToggleButtonGroup
                  value={stateNumberOfQuestions}
                  exclusive
                  onChange={(event, value) => setNumberOfQuestions(value)}
                  aria-label='number of questions'
                  className='w-full'
                >
                  <ToggleButton value={5} aria-label='5' className='w-full' disabled={ stateGeneralCollection.length < 5 }>
                    <b className='text-xl'>5</b>
                  </ToggleButton>
                  <ToggleButton value={15} aria-label='15' className='w-full' disabled={ stateGeneralCollection.length < 15 }>
                    <b className='text-xl'>15</b>
                  </ToggleButton>
                  <ToggleButton value={25} aria-label='25' className='w-full' disabled={ stateGeneralCollection.length < 25 }>
                    <b className='text-xl'>25</b>
                  </ToggleButton>
                  <ToggleButton value={50} aria-label='50' className='w-full' disabled={ stateGeneralCollection.length < 50 }>
                    <b className='text-xl'>50</b>
                  </ToggleButton>
                </ToggleButtonGroup>
              </FormControl>
            </div>

            <Button
              variant='contained'
              disableElevation
              color='secondary'
              size='large'
              className='w-full'
              onClick={() => {
                setSetup(false)
                setTest(true)
              }}
              disabled={ stateCollectionsValue.length === 0 || stateNumberOfQuestions === null }
            >
              Start testing
            </Button>
          </div>
      }
    </>
  )
}