import { useState, Fragment } from 'react'
import { Helmet } from 'react-helmet'
import {
  Select, MenuItem, ListSubheader, InputLabel, FormControl,
  ToggleButtonGroup, ToggleButton
} from '@material-ui/core'
import cn from 'classnames'
import { v4 as uuid } from 'uuid'

import { useCollections } from '../collections/CollectionsProvider'
import { LearningCard, SimpleLearningCard } from '../cards/LearningCard'

export default function Learning() {
  const [stateCollectionData, setCollectionData] = useState([])
  const [stateCollection, setCollection] = useState('')
  const [stateKanjiMode, setKanjiMode] = useState('visible')
  const [stateKanaMode, setKanaMode] = useState('basic')
  const [stateLearningSection, setLearningSection] = useState('hiragana')
  
  const { collections, learningCollections } = useCollections()

  const colors = ['red', 'yellow', 'purple', 'green', 'blue']

  function renderEmptyRow(len, className) {
    let row = []

    for (let i = 0; i < len; i++) {
      row.push(<div key={ uuid() }>&nbsp;</div>)
    }

    return (
      <div
        key={ uuid() }
        className={cn('', className)}
      >
        { row }
      </div>
    )
  }

  function renderKanaTableRows(collection, color, className, collapse = false) {
    return collection.grid.reduce((acc, row, rowIdx) => (
      [
        ...acc,
        <div
          key={ uuid() }
          className={cn('', className)}
        >
          {
            row.map((cell, cellIdx) => {
              const data = collection.data.find(item => item.en[0] === cell)

              return data
                ? <SimpleLearningCard
                    key={ cellIdx }
                    data={ data }
                    className={`
                      bg-${color}-100
                      hover:bg-red-200
                      text-${color}-800
                      hover:text-red-800
                    `}
                    extended={ ['nigori', 'extended'].indexOf(stateKanaMode) > -1 }
                  />
                : collapse ? null : <div key={ cellIdx } >&nbsp;</div>
            })
          }
        </div>
      ]
    ), []) 
  }

  function renderKanaTable(collection) {
    const rows = renderKanaTableRows(learningCollections[collection], 'green',
      'grid auto-rows-fr md:grid-cols-11 gap-1 lg:gap-2'
    )

    if (stateKanaMode === 'extended') {
      /* Desktop */
      rows.splice(2, 0, ...renderKanaTableRows(learningCollections[collection].extended, 'yellow',
        'hidden md:grid md:grid-cols-11 gap-0.5 lg:gap-2'
      ))
      
      /* Mobile */
      const extra = renderKanaTableRows(learningCollections[collection].extended, 'yellow',
        'grid md:hidden grid-rows-7 gap-0.5 lg:gap-2',
        true
      )
      extra.splice(1, 0, renderEmptyRow(7, 'grid-rows-7 md:grid-rows-none md:grid-cols-7 gap-1 lg:gap-2 grid md:hidden'))
      extra.splice(3, 0, renderEmptyRow(7, 'grid-rows-7 md:grid-rows-none md:grid-cols-7 gap-1 lg:gap-2 grid md:hidden'))
      rows.push(...extra)
    }

    return <div className='grid grid-cols-5 md:grid-cols-none md:grid-flow-row md:auto-rows-max gap-1 lg:gap-2'>
      { rows }
    </div>
  }

  return (
    <>
      <Helmet>
        <title>正解 (Seikai) | Learning</title>
      </Helmet>
      <div className='w-full md:w-1/2 lg:w-1/3 mx-auto mb-8'>
        <div className='mb-10'>
          <FormControl className='w-full'>
            <label
              id='learning-section-label'
              className='mb-1 text-sm text-gray-500'
            >
              Learning Section
            </label>
            <ToggleButtonGroup
              aria-labelledby='learning-section-label'
              value={stateLearningSection}
              exclusive
              onChange={(event, value) => { if (value !== null) setLearningSection(value) }}
              className='w-full'
            >
              <ToggleButton value='hiragana' aria-label='' className='w-full'>
                ひらがな
              </ToggleButton>
              <ToggleButton value='katakana' aria-label='' className='w-full'>
                カタカナ
              </ToggleButton>
              <ToggleButton value='kanji' aria-label='' className='w-full'>
                漢字
              </ToggleButton>
            </ToggleButtonGroup>
          </FormControl>
        </div>
        {
          ['hiragana', 'katakana'].indexOf(stateLearningSection) > -1 &&
            <div className='mb-10'>
              <FormControl className='w-full'>
                <label
                  id='kana-mode-label'
                  className='mb-1 text-sm text-gray-500'
                >
                  Kana mode
                </label>
                <ToggleButtonGroup
                  aria-labelledby='kana-mode-label'
                  value={stateKanaMode}
                  exclusive
                  onChange={(event, value) => { if (value !== null) setKanaMode(value) }}
                  className='w-full'
                >
                  <ToggleButton value='basic' aria-label='' className='w-full'>
                    Basic
                  </ToggleButton>
                  <ToggleButton value='nigori' aria-label='' className='w-full'>
                    + Nigori
                  </ToggleButton>
                  <ToggleButton value='extended' aria-label='' className='w-full'>
                    + Vowels
                  </ToggleButton>
                </ToggleButtonGroup>
              </FormControl>
            </div>
        }
        {
          stateLearningSection === 'kanji' &&
            <>
              <div className='mb-4'>
                <FormControl
                  variant='outlined'
                  className='w-full'
                >
                  <InputLabel id='collection-label'>Collection</InputLabel>
                  <Select
                    labelId='collection-label'
                    defaultValue=''
                    value={ stateCollection || '' }
                    label='Collection'
                    onChange={event => {
                      setCollection(event.target.value)
                      setCollectionData(collections.data[event.target.value])
                    }}
                  >
                    {
                      collections.index.filter(group => group.label !== 'ABC').reduce((acc, group) => {
                        return [
                          ...acc,
                          <ListSubheader key={ group.label }>{ group.label }</ListSubheader>,
                          group.options.map((option, optionIdx) => (
                            <MenuItem key={ optionIdx } value={ option.value }>
                              { option.label }
                            </MenuItem>
                          ))
                        ]
                      }, [])
                    }
                  </Select>
                </FormControl>
              </div>
              <div>
                <FormControl className='w-full'>
                  <label
                    id='kanji-mode-label'
                    className='mb-1 text-sm text-gray-500'
                  >
                    Kanji mode
                  </label>
                  <ToggleButtonGroup
                    aria-labelledby='kanji-mode-label'
                    value={stateKanjiMode}
                    exclusive
                    onChange={(event, value) => { if (value !== null) setKanjiMode(value) }}
                    className='w-full'
                  >
                    <ToggleButton value='visible' aria-label='visible mode' className='w-full'>
                      Visible
                    </ToggleButton>
                    <ToggleButton value='hidden' aria-label='hidden mode' className='w-full'>
                      Hidden
                    </ToggleButton>
                  </ToggleButtonGroup>
                </FormControl>
              </div>
            </>
        }
      </div>
      {
        (stateCollectionData.length === 0 && stateLearningSection === 'kanji') &&
          <div
            className='text-3xl italic text-center text-gray-400'
          >
            Please, select collection to learn
          </div>
      }
      {
        stateLearningSection === 'kanji' &&
          <div className='grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4'>
            {
              stateCollectionData.map((card, idx) => (
                <LearningCard
                  key={ idx }
                  data={ card }
                  blind={ stateKanjiMode === 'hidden' }
                  className={`
                    bg-gradient-to-r
                    from-${colors[idx % colors.length]}-200
                    to-${colors[(idx + 4) % colors.length]}-200
                    text-${colors[(idx + 3) % colors.length]}-900
                  `}
                />
              ))
            }
          </div>
      }
      {
        ['hiragana', 'katakana'].indexOf(stateLearningSection) > -1 &&
          renderKanaTable(stateLearningSection)
      }
    </>
  )
}