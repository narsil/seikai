import { Link } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import cn from 'classnames'

function Home() {
  const sections = [
    {
      to: '/learning',
      text: 'Start learn kana and kanji'
    },
    {
      to: '/training',
      text: 'Practice'
    },
    {
      to: '/testing',
      text: 'Test your knowledge'
    }
  ]
  const colors = ['red', 'yellow', 'blue', 'green', 'purple', 'pink']

  return (
    <>
      <Helmet>
        <title>正解 (Seikai) | Home page</title>
      </Helmet>
      <div className='flex flex-col justify-center h-full'>
        <h1 className='table mb-8 text-3xl md:text-7xl text-center'>
          <span className='inline-block pt-4 pb-6 bg-gradient-to-r from-blue-400 to-green-400 rounded-xl text-white'>
            &nbsp;正解 (Seikai)&nbsp;
          </span>
        </h1>
        <p className='mb-20 text-2xl text-center'>
          The simple and fast web application for learning and practicing your <span className='inline-block pb-1 bg-gradient-to-r from-red-400 to-yellow-400 rounded-lg text-white'>&nbsp;&nbsp;japanese&nbsp;&nbsp;</span> language!
        </p>
        <div className='flex flex-col md:flex-row items-stretch w-full rounded-xl overflow-hidden'>
          {
            sections.map((section, idx) => (
              <Link
                key={idx}
                to={section.to}
                className={cn(
                  'flex justify-center items-center w-full py-6 bg-gradient-to-r text-xl text-white text-center',
                  `from-${colors[idx * 2 + 0]}-400 to-${colors[idx * 2 + 1]}-400`,
                  `hover:from-${colors[idx * 2 + 0]}-500 hover:to-${colors[idx * 2 + 1]}-500`,
                  {
                    'border-t md:border-l md:border-t-0 border-white': idx 
                  }
                )}
              >
                {section.text}
              </Link>
            ))
          }
        </div>
      </div>
    </>
  )
}

export default Home