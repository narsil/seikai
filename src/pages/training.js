import { useEffect, useState } from 'react'
import {
  Select, MenuItem, ListSubheader, InputLabel, FormControl,
  ToggleButtonGroup, ToggleButton
} from '@material-ui/core'
import { Helmet } from 'react-helmet'

import { TrainingCard } from '../cards/TrainingCard'
import { CreateCardElements } from '../cards/CreateCardElements'
import { pickElement } from '../helpers/array'
import { useCollections } from '../collections/CollectionsProvider'

export default function Training() {
  const [stateTotalAnswers, setTotalAnswers] = useState(0)
  const [stateCorrectAnswers, setCorrectAnswers] = useState(0)

  const [stateMode, setMode] = useState('jp-en')
  const [stateSpelling, setSpelling] = useState('transliteration')
  const [stateDifficulty, setDifficulty] = useState(2)
  const [stateCollection, setCollection] = useState()
  const [stateCollectionData, setCollectionData] = useState([])
  
  const [stateCardElements, setCardElements] = useState(null)
  const { collections } = useCollections()
  
  useEffect(() => {
    if (stateCollectionData.length > 0) {
      const pick = pickElement(stateCollectionData)
      let picks = [pick]
  
      for (let i = 0; i < stateDifficulty - 1; i++) {
        picks.push(pickElement(stateCollectionData, picks));
      }

      const cardElements = (new CreateCardElements({ picks, mode: stateMode, spelling: stateSpelling })).exportTrainingCardElements()

      setCardElements(cardElements)
    }
  }, [stateTotalAnswers, stateCollectionData, stateDifficulty, stateMode, stateSpelling])


  function checkAnswerHandler(isCorrect) {
    setTotalAnswers(stateTotalAnswers + 1)
    if (isCorrect) {
      setCorrectAnswers(stateCorrectAnswers + 1)
    }
  }

  return (
    <>
      <Helmet>
        <title>正解 (Seikai) | Training</title>
      </Helmet>
      <div className='w-full md:w-1/2 lg:w-1/3 mx-auto'>
        <div className='mb-4'>
          <FormControl className='w-full'>
            <label className='mb-1 text-sm text-gray-500'>Mode</label>
            <ToggleButtonGroup
              value={stateMode}
              exclusive
              onChange={(event, value) => { if (value !== null) setMode(value) }}
              aria-label='difficulty'
              className='w-full'
            >
              <ToggleButton value='jp-en' aria-label='japanese mode' className='w-full'>
                日本語
              </ToggleButton>
              <ToggleButton value='en-jp' aria-label='english mode' className='w-full'>
                English
              </ToggleButton>
            </ToggleButtonGroup>
          </FormControl>
        </div>

        <div className='mb-4'>
          <FormControl className='w-full'>
            <label className='mb-1 text-sm text-gray-500'>Spelling</label>
            <ToggleButtonGroup
              value={stateSpelling}
              exclusive
              onChange={(event, value) => { if (value !== null) setSpelling(value) }}
              aria-label='spelling'
              className='w-full'
            >
              <ToggleButton value='transliteration' aria-label='transliteration' className='w-full'>
                Transliteration
              </ToggleButton>
              <ToggleButton value='native' aria-label='native' className='w-full'>
                Native
              </ToggleButton>
            </ToggleButtonGroup>
          </FormControl>
        </div>

        <div className='mb-10'>
          <FormControl className='w-full'>
            <label className='mb-1 text-sm text-gray-500'>Difficulty</label>
            <ToggleButtonGroup
              value={stateDifficulty}
              exclusive
              onChange={(event, value) => { if (value !== null) setDifficulty(value) }}
              aria-label='difficulty'
              className='w-full'
            >
              <ToggleButton value={2} aria-label='easy' className='w-full'>
                Easy
              </ToggleButton>
              <ToggleButton value={3} aria-label='medium' className='w-full'>
                Medium
              </ToggleButton>
              <ToggleButton value={4} aria-label='hard' className='w-full'>
                Hard
              </ToggleButton>
            </ToggleButtonGroup>
          </FormControl>
        </div>

        <div className='mb-8'>
          <FormControl
            variant='outlined'
            className='w-full'
          >
            <InputLabel id='collection-label'>Collection</InputLabel>
            <Select
              labelId='collection-label'
              defaultValue=''
              value={ stateCollection || '' }
              label='Collection'
              onChange={event => {
                setCollection(event.target.value)
                setCollectionData(collections.data[event.target.value])
              }}
            >
              {
                collections.index.reduce((acc, group) => {
                  return [
                    ...acc,
                    <ListSubheader key={ group.label }>{ group.label }</ListSubheader>,
                    group.options.map((option, optionIdx) => (
                      <MenuItem key={ optionIdx } value={ option.value }>
                        { option.label }
                      </MenuItem>
                    ))
                  ]
                }, [])
              }
            </Select>
          </FormControl>
        </div>
        
        {
          stateCardElements === null &&
          <div
            className='text-3xl italic text-center text-gray-400'
          >
            Please, select collection to practice
          </div>
        }
        {
          stateCardElements !== null &&
            <div className='relative'>
              <div className='absolute w-full -top-1'>
                <div className='table mx-auto px-2 py-1 rounded-md border border-white bg-gradient-to-r from-green-200 to-blue-200 text-md text-blue-900'>
                  Score: <b>{ stateTotalAnswers } / { stateCorrectAnswers }</b>
                </div>
              </div>
              <div className='flex justify-center'>
                <div className='w-full'>
                  <TrainingCard
                    dir={ 'col'/* stateMode === 'jp-en' ? 'col' : 'row' */ }
                    data={stateCardElements}
                    checkAnswerHandler={checkAnswerHandler}
                  />
                </div>
              </div>
            </div>
        }
      </div>
    </>
  )
}