import cn from 'classnames'
import { CreateCardElements } from '../cards/CreateCardElements'

export function ResultsTable({
  cards = [],
  answers = []
}) {
  return (
    <div className={cn(
      'w-full rounded-xl overflow-hidden',
      
    )}>
      {
        cards.map((card, cardIdx) => {
          const { riddle, options, indexOfCorrect } = CreateCardElements.prototype.exportResultsCardElements.call(card)

          return (
            <div
              key={cardIdx}
              className={cn(
                'flex flex-wrap w-full bg-gradient-to-r text-indigo-900',
                {
                  'border-t-8 border-white': cardIdx,
                  'from-blue-100 to-blue-400': cardIdx % 2 === 0,
                  'from-purple-100 to-purple-400': cardIdx % 2 === 1
                }
              )}
            >
              <div
                className='flex justify-center items-center w-full md:w-1/3 h-20 md:h-auto'
              >
                { riddle }
              </div>
              <div
                className='grid grid-flow-row auto-rows-fr md:w-2/3 w-full border-t md:border-t-0 md:border-l border-white'
              >
                {
                  options.map((option, optIdx) => (
                    <div
                      key={optIdx}
                      className={cn(
                        'flex justify-center items-center w-full h-20 md:h-auto',
                        {
                          'border-t border-white': optIdx,
                          'bg-gradient-to-r from-green-100 to-green-300 text-green-800': indexOfCorrect === optIdx,
                          'bg-gradient-to-r from-red-100 to-red-300 text-red-700': answers[cardIdx] === optIdx && indexOfCorrect !== optIdx
                        },
                      )}
                    >
                      { option }
                    </div>
                  ))
                }
              </div>
            </div>
          )
        })
      }
    </div>
  )
}