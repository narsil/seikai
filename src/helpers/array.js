// https://javascript.info/task/shuffle
export function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }

  return array
}

export function pickElement(arr, exclude) {
  if (arr.length === 0) return null

  if (!Array.isArray(exclude)) {
    return arr[Math.floor(Math.random() * arr.length)]
  } else {
    const p = arr[Math.floor(Math.random() * arr.length)]

    if (exclude.indexOf(p) === -1) {
      return p
    } else {
      return pickElement(arr, exclude)
    }
  }
}