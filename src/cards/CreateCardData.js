import { shuffle } from '../helpers/array'

export class CreateCardData {
  riddle = {}
  options = []
  indexOfCorrect = -1

  constructor({ picks, mode, spelling }) {
    this.picks = picks
    this.mode = mode
    this.spelling = spelling

    if (/^[a-z]{2}-[a-z]{2}$/.test(this.mode)) {
      this.modeRiddle = mode.split('-')[0]
      this.modeOptions = mode.split('-')[1]
      this.createTrainingData()
    }

    if (['kana', 'kanji'].indexOf(this.mode) > -1) {
      this.createTestingData()
    }
  }

  createTrainingData() {
    switch (this.modeRiddle) {
      case 'jp':
        this.riddle = {
          jp: this.picks[0].jp,
          onyomi: this.picks[0].onyomi?.[this.spelling],
          kunyomi: this.picks[0].kunyomi?.[this.spelling]
        }
      break

      case 'en':
        this.riddle = {
          en: this.picks[0].en,
          onyomi: this.picks[0].onyomi?.[this.spelling],
          kunyomi: this.picks[0].kunyomi?.[this.spelling]
        }
      break

      default: 
        this.riddle = {}
      break
    }

    switch (this.modeOptions) {
      case 'jp':
        this.options = shuffle(this.picks.map((pick, idx) => ({
          isCorrect: !idx,
          jp: pick.jp,
          onyomi: pick.onyomi?.[this.spelling],
          kunyomi: pick.kunyomi?.[this.spelling]
        })))
      break

      case 'en':
        this.options = shuffle(this.picks.map((pick, idx) => ({
          isCorrect: !idx,
          en: pick.en,
          onyomi: pick.onyomi?.[this.spelling],
          kunyomi: pick.kunyomi?.[this.spelling]
        })))
      break

      default:
        this.options = []
      break
    }

    this.indexOfCorrect = this.options.findIndex(opt => opt.isCorrect)
  }

  createTestingData() {
    switch (this.mode) {
      case 'kana':
        this.riddle = {
          en: this.picks[0].en,
          onyomi: this.picks[0].onyomi?.native,
          kunyomi: this.picks[0].kunyomi?.native
        }
        this.options = shuffle(this.picks.map((pick, idx) => ({
          isCorrect: !idx,
          jp: pick.jp,
        })))
      break

      case 'kanji':
        this.riddle = {
          jp: this.picks[0].jp,
        }
        this.options = shuffle(this.picks.map((pick, idx) => ({
          isCorrect: !idx,
          en: pick.en,
          onyomi: pick.onyomi?.native,
          kunyomi: pick.kunyomi?.native
        })))
      break
    
      default:
        this.riddle = {}
        this.options = []
      break
    }

    this.indexOfCorrect = this.options.findIndex(opt => opt.isCorrect)
  }
}