import cn from 'classnames'

function TrainingCard({
  data = null,
  checkAnswerHandler,
  dir = 'row'
}) {
  if (data === null) {
    return <div className='w-full text-center'>No data provided for card</div>
  }

  return (
    <div
      className='w-full rounded-xl overflow-hidden'
      style={{ height: '500px' }}
    >
      <div className={cn(
        'flex justify-center items-center bg-gradient-to-r from-yellow-200 to-red-200 text-red-900',
        {
          'h-2/3': dir === 'row',
          'h-1/3': dir === 'col'
        }
      )}>
        { data.riddle }
      </div>
      <div className={cn(
        'grid border-t border-white',
        {
          'grid-flow-col auto-cols-fr h-1/3': dir === 'row',
          'grid-flow-row auto-rows-fr h-2/3': dir === 'col'
        }
      )}>
        {
          data.options.map((option, idx) => (
            <div
              key={ Math.random().toString() }
              className={cn(
                `flex justify-center items-center
                bg-gradient-to-r from-green-200 to-blue-200 hover:from-green-300 hover:to-blue-300
                text-blue-900 cursor-pointer`,
                { 'border-l border-white': idx > 0 && dir === 'row' },
                { 'border-t border-white': idx > 0 && dir === 'col' }
              )}
              onMouseUp={() => checkAnswerHandler(idx === data.indexOfCorrect, idx)}
            >
              { option }
            </div>    
          ))
        }
      </div>
    </div>
  )
}

export { TrainingCard }