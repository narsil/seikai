import cn from 'classnames'

function TestingCard({
  data = null,
  checkAnswerHandler
}) {
  if (data === null) {
    return <div className='w-full text-center'>No data provided for card</div>
  }

  return (
    <div
      className='w-full rounded-xl overflow-hidden'
      style={{ height: '550px' }}
    >
      <div className='flex justify-center items-center h-1/3 bg-gradient-to-r from-yellow-200 to-red-200 text-red-900'>
        { data.riddle }
      </div>
      <div className='grid grid-flow-row auto-rows-fr h-2/3 border-t border-white'>
        {
          data.options.map((option, idx) => (
            <div
              key={ Math.random().toString() }
              className={cn(
                `flex justify-center items-center
                bg-gradient-to-r from-green-200 to-blue-200 hover:from-green-300 hover:to-blue-300
                text-blue-900 cursor-pointer`,
                { 'border-t border-white': idx > 0 }
              )}
              onMouseUp={() => checkAnswerHandler(idx === data.indexOfCorrect, idx)}
            >
              { option }
            </div>    
          ))
        }
      </div>
    </div>
  )
}

export { TestingCard }