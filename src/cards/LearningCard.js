import cn from 'classnames'

function LearningCard({
  data,
  blind,
  className
}) {
  return (
    <div
      className={cn(
        'group flex flex-col w-full rounded-lg cursor-pointer overflow-hidden',
        className
      )}
    >
      <div
        className='flex justify-center items-center text-5xl'
        style={{ willChange: 'background-color', height: '90px' }}
      >
        <span className={cn(
          'shippori group-hover:opacity-100 transition-all duration-300',
          {
            'opacity-0': blind
          }
        )}>
          { data.jp }
        </span>
      </div>
      
      <div
        className='flex justify-center items-stretch border-t border-white'
        style={{ willChange: 'background-color', height: '120px' }}
      >
        <div className='w-1/2 p-2 border-r border-white'>
          <p className='mb-2 text-sm'>Onyomi:</p>
          <p>{ data?.onyomi.native || <>&mdash;</> }</p>
          <p>{ data?.onyomi.transliteration || <>&mdash;</> }</p>
        </div>
        <div className='w-1/2 p-2'>
          <p className='mb-2 text-sm'>Kunyomi:</p>
          <p>{ data?.kunyomi.native || <>&mdash;</> }</p>
          <p>{ data?.kunyomi.transliteration || <>&mdash;</> }</p>
        </div>
      </div>
      
      <div
        className='flex justify-center items-center border-t border-white'
        style={{ willChange: 'background-color', height: '40px' }}
      >
        <b>{ data.en }</b>
      </div>
    </div>
  )
}

function SimpleLearningCard({
  data,
  className,
  extended = false
}) {
  const rows = []
  const max = extended ? data.jp.length : 1

  for (let i = 0; i < max ; i++) {
    rows.push(
      <div
        key={ i }
        className={cn(
          'flex justify-center items-center w-full gap-1',
          `${ extended ? 'flex-row' : 'flex-col' }`
        )}
      >
        <div className={cn(
          'font-light break-normal whitespace-nowrap',
          `${ extended
            ? 'w-1/2 text-right text-sm sm:text-xl md:text-sm lg:text-xl xl:text-2xl'
            : 'w-full text-center text-lg sm:text-3xl md:text-2xl lg:text-3xl xl:text-5xl'
          }`
        )}>
          { data?.jp[i] }
        </div>
        <div className={cn(
          'font-light text-xs sm:text-lg md:text-sm lg:text-lg xl:text-xl break-normal whitespace-nowrap',
          `${ extended
            ? 'w-1/2 text-left'
            : 'w-full text-center'
          }`
        )}>
          { data?.en[i] }
        </div>
      </div>
    )
    
  }
  return (
    <div className={cn(
      'relative rounded-lg aspect-h-1 aspect-w-1 cursor-pointer',
      className
    )}>
      <div className='absolute w-full h-full flex flex-col justify-center items-center'>
        { rows }
      </div>
    </div>
  )
}

export { LearningCard, SimpleLearningCard }