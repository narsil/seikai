import { CreateCardData } from './CreateCardData'
import cn from 'classnames'

export class CreateCardElements extends CreateCardData {
  constructor({ picks, mode, spelling }) {
    super({ picks, mode, spelling })
  }

  exportResultsCardElements() {
    let $riddle
    let $options

    switch (this.mode) {
      case 'kana':
        $riddle =
          <div className='w-full px-4'>
            <div className='text-sm font-bold'>{ this.riddle.en }</div>
            {
              (!!this.riddle.onyomi) &&
                <div className='text-md'>
                  { this.riddle.onyomi || null }
                </div>
            }
            {
              (!!this.riddle.kunyomi) &&
                <div className='text-md'>
                  { this.riddle.kunyomi || null }
                </div>
            }
          </div> 
        $options = this.options.map((opt, idx) => (
          <div className='w-full px-2 py-1 text-3xl' key={idx}>
            { opt.jp }
          </div>
        ))
      break
      
      case 'kanji':
        $riddle =
          <span className='text-2xl'>
            { this.riddle.jp }
          </span>
        $options = this.options.map((opt, idx) => (
          <div className='w-full px-2 py-1' key={idx}>
            { opt.en }
            {
              (!!opt.onyomi) &&
                <span className='text-sm border-l border-current ml-2 pl-2'>
                  { opt.onyomi || null }
                </span>
            }
            {
              (!!opt.kunyomi) &&
                <span className='text-sm border-l border-current ml-2 pl-2'>
                  { opt.kunyomi || null }
                </span>
            }
          </div>
        ))
      break
      
      default:
      break
    }

    return { 
      riddle: $riddle,
      options: $options,
      indexOfCorrect: this.indexOfCorrect
    }
  }

  exportTrainingCardElements() {
    let $riddle
    let $options

    switch (this.mode) {
      case 'jp-en':
        $riddle =
          <div className='text-5xl text-center'>
            { this.riddle.jp }
          </div>
        $options = this.options.map((opt, idx) => (
          <div className='w-full px-4'>
            <b>{ opt.en }</b>
            {
              (opt.onyomi !== undefined || opt.kunyomi !== undefined) &&
                <div className='text-sm'>
                  Onyomi:&nbsp;&nbsp;&nbsp;{ opt.onyomi || null }
                  <br/>
                  Kunyomi:&nbsp;&nbsp;&nbsp;{ opt.kunyomi || null }
                </div>
            }
          </div>
        ))
      break

      case 'en-jp':
        $riddle =
          <div className='w-full px-4'>
            <div className='mb-2 text-2xl text-center'>
              <b>{ this.riddle.en }</b>
            </div>
            {
              (this.riddle.onyomi !== undefined || this.riddle.kunyomi !== undefined) &&
                <>
                  Onyomi:&nbsp;&nbsp;&nbsp;{ this.riddle.onyomi || null }
                  <br/>
                  Kunyomi:&nbsp;&nbsp;&nbsp;{ this.riddle.kunyomi || null }
                </>
            }
          </div>
        $options = this.options.map((opt, idx) => (
          <div className='flex items-center text-4xl'>
            { opt.jp }
          </div>
        ))
      break

      default:
      break
    }
    
    return { 
      riddle: $riddle,
      options: $options,
      indexOfCorrect: this.indexOfCorrect
    }
  }

  exportTestingCardElements({ translation }) {
    let $riddle
    let $options

    switch (this.mode) {
      case 'kana':
        $riddle =
          <div className='text-left'>
            {
              translation &&
                <div className='text-lg text-center font-bold'>
                  { this.riddle.en }
                </div>
            }
            {
              (!!this.riddle.onyomi) &&
                <div className={cn(
                  'text-center text-3xl',
                  {
                    'border-t border-white': translation
                  }
                )}>
                  { this.riddle.onyomi || null }
                </div>
            }
            {
              (!!this.riddle.kunyomi) &&
                <div className={cn(
                  'text-center text-3xl',
                  {
                    'border-t border-white': this.riddle.onyomi || translation
                  }
                )}>
                  { this.riddle.kunyomi || null }
                </div>
            }
          </div>
        $options = this.options.map((opt) => (
          <div className='text-3xl text-center w-full px-4 py-1 select-none' key={opt.jp}>
            { opt.jp }
          </div>
        ))
      break

      case 'kanji':
        $riddle =
          <div className='text-4xl text-center'>
            { this.riddle.jp }
          </div>
        $options = this.options.map((opt, idx) => (
          <div className='px-8 select-none' key={idx}>
            {
              translation &&
                <div className='text-lg text-center font-bold'>
                  { opt.en }
                </div>
            }
            {
              (!!opt.onyomi) &&
                <div className={cn(
                  'text-2xl text-center',
                  {
                    'border-t border-white': translation
                  }
                )}>
                  { opt.onyomi || null }
                </div>
            }
            {
              (!!opt.kunyomi) &&
                <div className={cn(
                  'text-2xl text-center',
                  {
                    'border-t border-white': this.riddle.onyomi || translation
                  }
                )}>
                  { opt.kunyomi || null }
                </div>
            }
          </div>
        ))
      break

      default:
      break
    }
    
    return { 
      riddle: $riddle,
      options: $options,
      indexOfCorrect: this.indexOfCorrect
    }
  }
}