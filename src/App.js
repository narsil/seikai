import React from 'react'
import {
  HashRouter as Router,
  Switch,
  Route,
  Link,
  useLocation
} from 'react-router-dom'
import cn from 'classnames'
import { Helmet } from 'react-helmet'
import { unstable_createMuiStrictModeTheme as createMuiTheme } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'
import { teal } from '@material-ui/core/colors'

import { CollectionsProvider } from './collections/CollectionsProvider'
import Home from './pages/index'
import Learning from './pages/learning'
import Training from './pages/training'
import Testing from './pages/testing'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome, faBookReader, faUserGraduate, faBookOpen } from '@fortawesome/free-solid-svg-icons'

const theme = createMuiTheme({
  palette: {
    primary: teal,
  },
  overrides: {
    MuiToggleButton: {
      root: {
        color: teal[500],
        backgroundColor: teal[50],
        '&$selected': {
          color: 'white',
          backgroundColor: teal[400],
          '&:hover': {
            backgroundColor: teal[400]
          }
        },
        '&:hover': {
          backgroundColor: teal[100]
        }
      },
      selected: {
      }
    }
  }
});

function Nav() {
  const location = useLocation()
  
  return (
    <footer className='fixed bottom-0 w-full border-t border-gray-300 bg-white'>
      <div className='container mx-auto py-3'>
        <nav className='w-full'>
          <ul className='flex justify-between'>
            <li className='px-3'>
              <Link
                to='/'
                className={cn(
                  'text-2xl',
                  {'text-red-500': location.pathname === '/'}
                )}
              >
                <FontAwesomeIcon icon={faHome}/>
              </Link>
            </li>
            <li className='px-3'>
              <Link
                to='/learning'
                className={cn(
                  'text-2xl',
                  {'text-red-500': location.pathname === '/learning'}
                )}
              >
                <FontAwesomeIcon icon={faBookOpen}/>
              </Link>
            </li>
            <li className='px-3'>
              <Link
                to='/training'
                className={cn(
                  'text-2xl',
                  {'text-red-500': location.pathname === '/training'}
                )}
              >
                <FontAwesomeIcon icon={faBookReader}/>
              </Link>
            </li>
            <li className='px-3'>
              <Link
                to='/testing'
                className={cn(
                  'text-2xl',
                  {'text-red-500': location.pathname === '/testing'}
                )}
              >
                <FontAwesomeIcon icon={faUserGraduate}/>
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </footer>
  )
}

function App() {
  return (
    <CollectionsProvider>
      <ThemeProvider theme={theme}>
        <Helmet>
          <title>正解 (Seikai)</title>
        </Helmet>
        <Router>
          <div className='app-container'>
            <main className='pt-8 pb-24 bg-white'>
              <div className='container min-h-full h-full mx-auto px-2'>
                <Switch>
                  <Route exact path='/'>
                    <Home/>
                  </Route>
                  <Route exact path='/learning'>
                    <Learning/>
                  </Route>
                  <Route exact path='/training'>
                    <Training/>
                  </Route>
                  <Route exact path='/testing'>
                    <Testing/>
                  </Route>
                </Switch>
              </div>
            </main>  
            <Nav/>
          </div>
        </Router>
      </ThemeProvider>
    </CollectionsProvider>
  );
}

export default App